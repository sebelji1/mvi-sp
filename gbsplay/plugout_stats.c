/*
 * gbsplay is a Gameboy sound player
 *
 * 2006 (C) by Tobias Diedrich <ranma+gbsplay@tdiedrich.de>
 *
 * Licensed under GNU GPL v1 or, at your option, any later version.
 */

#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#include "plugout.h"

/*
XXX 21?

ln2 = .69314718055994530941
magic = 5.78135971352465960412

def to_midi(x):
    return round((math.log(262144 / x)/ln2 - magic)*12 + 21)

def from_midi(y):
	return math.e**(math.log(262144) - ((y - 21) / 12 + magic) * ln2)

*/

#define LN2 .69314718055994530941
#define MAGIC 5.78135971352465960412
#define FREQ(x) (262144 / (x))
#define NOTE(x) ((long)((log(FREQ(x))/LN2 - MAGIC)*12 + .2))

int frame = 0;

static long stats_open(enum plugout_endian endian, long rate, long *buffer_bytes)
{
	printf("! Simple");
	return 0;
}

static ssize_t stats_write(const void *buf, size_t count)
{
	return 0;
}

static int stats_skip(int subsong)
{
	printf("\n! Song %d", subsong);
	frame = 0;
	return 0;
}

static void stats_close()
{
	printf("\n! End\n");
}

int prev_note = 0;
int prev_note_played = false;
int pause_frames = 0;

static int stats_vblank(long cycles, const struct gbhw_channel chan[])
{
	int n, note, new_playing;
	const struct gbhw_channel *ch;

	/*
	for (c = 0; c < 3; c++) {
		ch = &chan[c];
	}
	*/

	ch = &chan[0];
	note = NOTE(ch->div_tc);
	new_playing = ch->running && ch->master && ch->env_volume;
	n = 0;
	if (note != prev_note) {
		n = note;
		prev_note = note;
		prev_note_played = true;
	} else if (note == prev_note && prev_note_played == false && new_playing == true) {
		n = note;
		prev_note = note;
		prev_note_played = true;
	} else if (note == prev_note && new_playing == false) {
		prev_note_played = false;
	}

	
	if (frame % 30 == 0) {
		printf("\n");
	}

	if (n == 0) {
		printf(".. ");
		pause_frames++;
	} else {
		//printf("0*%d\n", pause_frames);
		printf("%2d ", n);
		pause_frames = 0;
	}

	frame++;
	
	return 0;
}

const struct output_plugin plugout_stats = {
	.name = "stats",
	.description = "Export some statistics",
	.open = stats_open,
	.write = stats_write,
	.close = stats_close,
	.skip = stats_skip,
	.vblank = stats_vblank,
	.flags = PLUGOUT_USES_STDOUT,
};
